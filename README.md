# Back-End Challenge

## Overview

In index.js, make the handler function return an array of the glossary items from healthcare.gov (the url is specified in index.js).

- If a language is specified as an argument, only return items for that language
- If a language is not specified, return all items.

## Directions

- clone this repository
- checkout a new branch
- `npm install`
- make any changes you need to meet the requirements
- run `npm test` to make sure the tests pass
- open a pull request when you are ready for your solution to be reviewed

## Notes

- use async / await
- there's no need to spend a lot of time on this; aim for 30 minutes or less
- this is the first iteration of our code challenge—let us know if you have any suggestions!
