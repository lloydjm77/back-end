const chai = require('chai');
const index = require('./index');

const expect = chai.expect;

describe('Tests for Healthcare.gov code challenge', function() {
  describe('handler function', function() {
    it('should return an array of english articles when requested', async function() {
      const result = await index.handler('en');
      expect(result).to.be.an('array');
      result.forEach(item => expect(item.lang).to.equal('en'));
    });
    it('should return an array of spanish articles when requested', async function() {
      const result = await index.handler('es');
      expect(result).to.be.an('array');
      result.forEach(item => expect(item.lang).to.equal('es'));
    });
    it('should return an array of all articles when no language is specified', async function() {
      const result = await index.handler();
      let languages = new Set();
      result.forEach(item => languages.add(item.lang));
      expect(result).to.be.an('array');
      expect(result).to.not.be.empty;
      expect(languages).to.include('en');
      expect(languages).to.include('es');
    });
  });
});